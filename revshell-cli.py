#!/bin/env python3
import argparse
import json
import re

# Script made by 

def transform_cmd(cmd,shell,ip,port):
    # De Reverse Generica a Reverse con shell,ip y port
    
    cmd = cmd.replace("{shell}",shell)
    cmd = cmd.replace("{ip}",ip)
    cmd = cmd.replace("{port}",port)
    
    return cmd

def get_rev_list(tipo):
    # Generar lista de tipo shell
    rev_list = []

    for key in data:
        #print(tipo,key['name'])
        if re.match(tipo, key['name'],re.IGNORECASE):
            #print("match")
            rev_list.append(key['command'])

    if not rev_list:
        print("\t[-] Reverse not Found\n")

    return rev_list

def generate_shells(tipo,shell,ip,port):
    
    shells = get_rev_list(tipo)

    for cmd in shells:
        print(transform_cmd(cmd,shell,ip,port),'\n')



if __name__ == '__main__':
    
    parser = argparse.ArgumentParser(description='A Simple cli RevShell generator.\n\t\tData from: https://github.com/0dayCTF/reverse-shell-generator/')
    parser.add_argument('lang',action="store", help="Select code laguague from")
    parser.add_argument('shell',action="store", help="Kind of shell to invoque")
    parser.add_argument('ip',action="store", help="Your local ip address")
    parser.add_argument('port',action="store", help="Port to lisent")
    args = parser.parse_args()
    tipo = args.lang
    shell = args.shell
    ip = args.ip
    port = args.port


    if "meter" != tipo:
        data_source = 'data/general.json'
    else:
        data_source = 'data/meter.json'
        tipo = shell


    with open(data_source, "r") as read_file:
        data = json.load(read_file)

    generate_shells(tipo,shell,ip,port)

